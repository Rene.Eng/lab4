package cellular;

import java.io.ObjectInputStream.GetField;

public class BriansBrain extends GameOfLife {

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row, col) == CellState.ALIVE) return CellState.DYING;
        if (getCellState(row, col) == CellState.DYING) return CellState.DEAD;
        if (getCellState(row, col) == CellState.DEAD
        && countNeighbors(row, col, CellState.ALIVE) == 2)
            return CellState.ALIVE;
		return CellState.DEAD;
	}
}
